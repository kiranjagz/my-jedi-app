import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { LogService } from '../log/log.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ForceCharacter } from '../../create-character/ForceCharacter';

@Injectable()
export class StarWarsService {
  private characters = [];
  charactersChanged = new Subject<void>();

  constructor(private logService: LogService, private http: HttpClient) {
    this.logService = logService;
    this.http = http;
  }

  public fetchCharacters() : void {
    this.http
      .get('https://swapi.dev/api/people/')
      .pipe(
        map((res) => {
          const data = JSON.parse(JSON.stringify(res));
          const names = data.results;
          const chars = names.map((char: { name: any }) => {
            return { name: char.name, side: '' };
          });
          console.log(data);
          return chars;
        })
      )
      .subscribe((result) => {
        this.characters = result;
        this.charactersChanged.next();
      });
  }

  public getCharacters(chosenList: string) : any[] {
    if (chosenList === 'all') {
      return this.characters.slice();
    }
    return this.characters.filter((char) => {
      return char.side === chosenList;
    });
  }

  public onSideChosen(charinfo: { name: string; side: string }) : void {
    const pos = this.characters.findIndex((char) => {
      return char.name === charinfo.name;
    });
    this.characters[pos].side = charinfo.side;
    this.charactersChanged.next();
    this.logService.writeLog('Changed side of: ' + charinfo.name + ', new side: ' + charinfo.side);
  }

  public addCharacter(ForceCharacter: ForceCharacter) : boolean {
    const pos = this.characters.findIndex((char) => {
      return ForceCharacter.Name === char.name;
    });
    if (pos !== -1) {
      return false;
    }

    const newChar = { name: ForceCharacter.Name, side: ForceCharacter.Side };
    this.characters.push(newChar);
    return true;
  }
}
