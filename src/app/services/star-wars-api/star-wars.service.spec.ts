import { TestBed } from '@angular/core/testing';
import { StarWarsService } from './star-wars.service';
import { HttpClientModule } from '@angular/common/http';
import { LogService } from '../log/log.service';
import { ForceCharacter } from 'src/app/create-character/ForceCharacter';

describe('Star Wars Service Tests', () => {
  let service: StarWarsService;
  let logService: LogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [StarWarsService, LogService],
    });
    service = TestBed.inject(StarWarsService);
    logService = TestBed.inject(LogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('add a character', () => {
    let characters = ([] = [
      {
        name: 'Kiran',
        side: 'Light',
      },
      { name: 'Luke', side: 'Darl' },
    ]);

    let newForceCharacter: ForceCharacter = { Name: 'Bobbo', Side: 'Dark' };

    expect(service.addCharacter(newForceCharacter)).toEqual(true);
  });
});
