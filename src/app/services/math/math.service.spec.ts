import { TestBed } from '@angular/core/testing';
import { MathService } from './math.service';

describe('Math Service Tests', () => {
  let service: MathService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MathService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Add two postive numbers', () => {
    expect(service.add(1,5)).toEqual(6);
  });

  it('postive subract larger negative number', () => {
    expect(service.subtract(1,5)).toEqual(-4);
  });

  it('two postive numbers to subtract', () => {
    expect(service.subtract(10,5)).toEqual(5);
  });

});
