import { Component, OnInit } from '@angular/core';
import { StarWarsService } from './services/star-wars-api/star-wars.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'my-jedi-app';
  swService: StarWarsService;

  constructor(swService: StarWarsService){
    this.swService = swService;
  }

  ngOnInit(){
    this.swService.fetchCharacters();
  }
}
