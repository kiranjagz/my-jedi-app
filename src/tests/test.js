describe('Array', () => {
    describe('#indexOf()', () => {
        it('should return -1 when the value is not present', () => {
            assert.equal(-1, [1, 2, 3].indexOf(4));
        });
    });
});

describe('Add numbers', () => {
    describe('#addTwoNumbers(a,b)', () => {
        it('should return 1 when the value is present', () => {
            var result = 2 + 2;
            assert.equal(result, 4);
        });
    });
});